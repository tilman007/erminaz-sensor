#ifndef BG51_H
#define BG51_H

#define INTEGRATION_COUNT (10)  // tick count to measure radiation intensity through time

typedef struct bg51_radiation_s {
    uint32_t timestamp;			// timestamp [us] of the measurement (integration time might be huge!)
    uint32_t integration_time;	// time [us] to reach INTEGRATION_COUNT
    uint16_t counts;            // counts [n]
} bg51_radiation_t;

void bg51_callback(uint16_t n);
void bg51_service(void);

#endif
